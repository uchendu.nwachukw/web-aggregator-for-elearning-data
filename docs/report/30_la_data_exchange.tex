\chapter{Objective 2: Data Aggregation Module}

\minitoc

An objective of this project is the conception and creation of a prototype to aggregate learning data from an external \gls{lms} into Graasp. In this context, aggregation has to be understood mainly as secure transfer of data. Later the imported data will be processed to perform \gls{la} at a greater scale. 

The prototype may be seen as a proof of concept, whereby Moodle is used as external \gls{lms}. However, the selected architecture for the whole Data Aggregation Module can easily be extended to fetch data from other \glspl{lms}.

The sequence diagram in Figure \ref{fig:config_graasp_app_for_import_moodle} shows which interactions the Data Aggregation Module will handle.

\begin{figure}[H]
	\center
	\includegraphics[width=8.5cm]{img/diagrams/schema_import_communication.png}
	\caption{Interactions to perform the data import}
	\label{fig:config_graasp_app_for_import_moodle}
\end{figure}

\section{Architecture}
The Data Aggregation Module is composed of the Graasp Moodle App and an external \gls{lms}. The communication between the two parts is represented in Figure \ref{fig:dam_interaction_systems}. Displaying the external \gls{lms} as a black box is justified by the variations of internal architecture of open source and commercially hosted \glspl{lms}. 

\begin{figure}[h]
	\center
	\includegraphics[height=\schemagraaspheight]{img/diagrams/schema_communication_graasp_external_lms.png}
	\caption{General architecture of Data Aggregation Module}
	\label{fig:dam_interaction_systems}
\end{figure}

For the sake of simplicity, inherited components from the Graasp Starter App are omitted such as the \textit{TeacherMode} and \textit{TeacherView}. This keeps the focus on the relevant parts of the Data Aggregation Module.

The fact, that Moodle is used as external \gls{lms}, allows a more precise representation of the communication link, as shown in Figure \ref{fig:dam_interaction_moodle}. As can be seen, the \textit{MoodleAPIRequests} object is responsible for interacting with the target Moodle instance. On the other side, the Moodle Web Service Plugin takes care of gathering the requested information from the database and returning it.

\begin{figure}[h]
	\center
	\includegraphics[width=\textwidth]{img/diagrams/schema_communication_graasp_moodle.png}
	\caption{Interaction between \textit{APIRequests} class and external \gls{lms}}
	\label{fig:dam_interaction_moodle}
\end{figure}

If a different \gls{lms} should be used as data source, a new \textit{APIRequests} class shall be created. Its single purpose is the handling of the interaction between the Graasp App and the \gls{lms}. This may be over HTTPS but is not limited to it. How exactly the data is collected depends on the \gls{lms}. If a rich API is offered, often multiple calls to the API may suffice, but there are also cases where there is no out-of-the-box solution.

The definition and usage of \textit{APIRequests} follows a \gls{Strategy Pattern}. The principal workflow doesn't change and specific methods are accessible through an object to keep the component (\textit{Settings}) unpolluted by using a switch more than once. A detailed description how this pattern is used can be found in Section \ref{sec:moodle_api_requests_obj}. 
	

\section{Development Strategy} 
Throughout the development of the Data Aggregation Module, certain principles are followed while aiming to increase efficiency and improve code quality. The principles are based on project related guidelines, \gls{tdd} and personal experience. They are described in the following list and result combined in the development strategy applied:
\begin{itemize}
	\item \textbf{Implement Quickly, Refactor Later}: When implementing a new feature, the priority is first to achieve the desired output as quickly as possible. Then, in a second step, the implementation is refactored to match common coding guidelines and best practices. The origin of this principle can be found in \gls{tdd}, to which an introduction is given in the appendix \ref{appendix:tdd}.
	\item \textbf{Light Testing}: Even though it was foreseen to apply extensive testing and even \gls{tdd} during development, this would be a very time-consuming task in this particular case. This is mainly due to the calls to the API or database, which would require the usage of \gls{mocks and stubs} for testing. Finally, the benefit of the tests are insufficient to justify this instrument for a prototype. As a consequence, only an integration test is performed, which runs through the module from start to end.
	\item \textbf{Profit from Graasp App Standards}: For the development of the Graasp App, the defined best practices are followed. This includes: linters for Git commits (\textit{commitlint}) and JavasSript code (\textit{ESlint}), separation of concerns using \gls{Container Pattern} and CI/CD via Codeship.
	\item \textbf{Simplified Git-Flow Workflow}: Deviating from the GitLab Workflow applied in Graasp projects usually, Git-flow is chosen for this prototype to reduce complexity. As there is only a single contributer and no features are developed in parallel, the Git-flow Workflow is applied only partially. This means that there are only two branches:
	\begin{itemize}
		\item \textit{master}: which always contains a stable version that can be used for demonstration and deployment.
		\item \textit{dev}: which contains the latest development work.
	\end{itemize}
	\item \textbf{Code Review through Experts}: To assure quality code it is reviewed by experts. The client is willing to take on this responsibility for the Graasp App. For the Moodle plugin, it has to be estimated if a code review is required and if so, who could do it.
	\item \textbf{Keep It Simple}: Despite the over usage of those three words, they deserve to be mentioned. As there are various unknown variables in the project, only features demanded by the client should be implemented. Obviously, this doesn't prevent the suggestion of helpful features to the client.
\end{itemize}


\section{Docker Environment} \label{sec:docker_env}
A reliable environment to develop and run the Data Aggregation Module is provided in form of Linux Docker \cite{docker} containers. The environment consists of three containers as shown in Figure \ref{fig:docker_env_dam} and is assembled for convenience with \textit{docker-compose}. 

\begin{figure}[H]
	\center
	\includegraphics[width=.65\textwidth]{img/docker/schema_docker_dev_env.png}
	\caption{Docker environment for the Data Aggregation Module with port mapping as defined in \textit{docker-compose.yml}}
	\label{fig:docker_env_dam}
\end{figure}

The content of the three containers is described throughout the following sections and the limitations of this environment presented.

\input{33_docker_containers}


\section{Moodle Web Service Plugin}
The Moodle Web Service Plugin exposes two functions on a Moodle instance that can be invoked through HTTPS requests. Once the plugin is installed and the Moodle instance configured correctly, the functions can be invoked by calling the REST endpoint at \url{[yourMoodleUrl]/webservice/rest/server.php} and passing the function name as an argument for the parameter \textit{wsfunction} (\textit{web service function}).

The goal of the plugin is the retrieval of a list of courses available to a user. From this list, some courses can be selected and associated learning data can be fetched by an external application.

In the following sections, the conception, content and technical details of the plugin are discussed. The source code for the plugin may be found on the GitLab of HEIA-FR (\url{https://bit.ly/38MioWf}). 


\subsection{Motivation}
Retrieving the learning data through a plugin is the most convenient approach. Once the plugin is installed, the functions are directly exposed under a web service without further need for configuration from the Moodle administrator.

Moodle offers more than 100 in-built services \cite{moodledocs_ws_func}. They allow the retrieval of some learning data, but a custom function can exploit the Moodle Data Manipulation API, which gives full access to the database and therefore to all available learning data.
As long as there is no precise definition of the data to retrieve, the content of the activity log is returned.


\subsection{Plugin Content}
The plugin is composed by the three files (\textit{services.php}, \textit{externallib.php} and \textit{version.php}), which are described in the following sections by providing general info about their purpose and explanation of crucial code blocks. When a certain implementation is trivial, code is omitted, but can be found on GitLab.

The titles of each subsection is the relative location from the plugin directory of the file in question. Note that the construction of certain function names is imposed by Moodle and usually constituted of the plugin's name and the corresponding function. If the naming convention is violated, the plugin won't work.

\subsubsection{db/services.php}
This file declares the available functions. The display name of a function is mapped with meta information of the function (class name, method name, physical class location, description and type). Listing \ref{lst:declare_function} shows an example where the function to fetch the learning data for a course is declared.

\begin{lstlisting}[language=php, caption={Declare function to fetch learning data}, label={lst:declare_function}]
'local_wafed_moodle_webservice_plugin_get_course_data' => array(
  'classname'   => 'local_wafed_moodle_webservice_plugin_external',
  'methodname'  => 'get_course_data',
  'classpath'   => 'local/wafed_moodle_webservice_plugin/externallib.php',
  'description' => 'Return the log for the given course',
  'type'        => 'read',
),
\end{lstlisting}

Furthermore, the \textit{services.php} file is used to assemble multiple functions into a single web service. Listing \ref{lst:declare_service} shows an example, where two functions are assembled in the web service called \textit{TB WAFED Web Services}. The attributes \textit{restrictedusers} and \textit{enabled} are used to expose the service by upon installation to all users.

\begin{lstlisting}[language=php, caption={Assemble functions into a single web service}, label={lst:declare_service}]
'TB WAFED Web Services' => array(
  'functions' => array ('local_wafed_moodle_webservice_plugin_get_available_courses', 'local_wafed_moodle_webservice_plugin_get_course_data'),
  'restrictedusers' => 0,
  'enabled'=> 1,
  'shortname' => 'wafed_webservices'
)
\end{lstlisting}

A detailed explanation for this file can be found in the Moodle documentation under \url{https://docs.moodle.org/dev/Web_services_API}.

\subsubsection{externallib.php}
By looking again at Listing \ref{lst:declare_function}, it may be observed that the value at index \textit{classpath} is referencing this file.
The other indices \textit{classname} and \textit{methodname} are referencing a class and method in this particular file.

Thus, each function declared in \textit{service.php} maps to a single method of a class in the \textit{externallib.php} file. Each such method has two auxilliary methods used for validation and documentation generation. 
The first one, with the suffix \textit{\_parameters}, is shown in Listing \ref{lst:describe_params} and is used to describe the parameters that the method expects.

\begin{lstlisting}[language=php, caption={Method to describe expected parameters}, label={lst:describe_params}]
public static function get_course_data_parameters() {
  return new external_function_parameters(
    array(
      'courseids' => new external_multiple_structure(
        new external_value(PARAM_INT, 'the id of a course')
      )
    )
  );
}
\end{lstlisting}

The second auxilliary method is suffixed with \textit{\_returns} and describes the return values of the function. An example is shown in Listing \ref{lst:describe_return_values}. 

\begin{bclogo}[
	couleur=bglightgrey,
	arrondi=0,
	logo=\bcinfo,
	barre=none,
	marge=10,
	ombre=true,
	noborder=true]{Skip Unlisted Values and Error Throwing}
Values not listed in the \textit{\_returns} method are simply skipped and not returned. If there is a type error, an exception is returned. The same applies when using parameter validation, shown in Listing \ref{lst:param_validation}.
\end{bclogo}

\begin{lstlisting}[language=php, caption={Method to describe return value}, label={lst:describe_return_values}]
public static function get_course_data_returns() {
  return new external_multiple_structure(
    new external_single_structure(
      array(
        'action' => new external_value(PARAM_TEXT, 'the action type'),
        'target' => new external_value(PARAM_TEXT, 'the action aims on'),
        'userid' => new external_value(PARAM_INT, 'the users id'),
        ...
      )
    )
  );
}
\end{lstlisting}


\begin{lstlisting}[language=php, caption={Parameter validation}, label={lst:param_validation}]
$params = self::validate_parameters(self::get_course_data_parameters(),
  array(
    'courseids' => $courseids,
  ));
\end{lstlisting}

Method \textit{get\_course\_data} referenced in Listing \ref{lst:declare_function} attribute \textit{methodname} contains the code executed when calling the declared function. The signature and basic structure of the method is shown in Listing \ref{lst:meth_sig_and_content}. 

\begin{lstlisting}[language=php, caption={Principal method signature and structure}, label={lst:meth_sig_and_content}]
public static function get_course_data($courseids) {
  // 1. Parameter validation
  
  // 2. Context validation and capability checking
  
  // 3. Get roles of enrolled users per course
  
  // 4. Get course data from the log reader
  
  // 5. Enrich course data with users assigned role 
        
  // 6. Return results
}
\end{lstlisting}

Step 2 is context\footnote{There are different context levels in Moodle, at which roles and capabilities can be assigned. Examples for context levels are \textit{site}, \textit{course} or \textit{activity}. More information can be found under \url{https://docs.moodle.org/39/en/Context}.} validation, that allows to guarantee that the user executing the function has the necessary capabilities on this context level. 
Listing \ref{lst:cxt_validation_check_capability} shows an example, which checks for each course that the user can see the activity log.

\begin{lstlisting}[language=php, caption={Context validation and capability checking}, label={lst:cxt_validation_check_capability}]
foreach($courseids as $courseid){
  // Context validation
  $coursecontext = context_course::instance($courseid);
  self::validate_context($coursecontext);

  // Capability checking
  if (!has_capability('report/log:view', $coursecontext)) {
    throw new moodle_exception('You have not the required capabilities (report/log:view) to use this function.');
  }
}
\end{lstlisting}

Steps 3 to 5 are plugin specific. The associated code is self-explanatory and therefore left out of this section.


\subsubsection{version.php}
This file is used during installation and upgrade of the plugin. The most important information are about the current plugin version and the required Moodle version. Therefore, timestamps of the releases are used as identifiers instead of traditional version numbers. For instance, Moodle Version 3.0 has the timestamp \textit{2015111600}, whereby the last two digits are used for daily versions. The Listing \ref{lst:def_plug_vers} shows the definition for this particular plugin.

\begin{lstlisting}[language=php, caption={Plugin version and required Moodle version definition}, label={lst:def_plug_vers}]
$plugin->version  = 2020062601;
$plugin->requires = 2015111600;
\end{lstlisting}

During the installation or an upgrade, the current plugin version is stored in the database. When the version in the file is newer than the one stored, the plugin is upgraded. This is sometimes necessary to make visible modifications that involve changes of information stored in the database (e.g. function declarations). 

Information concerning the other options used in this file can be found under \url{https://docs.moodle.org/dev/version.php}.


\subsection{Installation}
Before installing the plugin, the target Moodle instance should be configured accordingly. This includes the enabling of web services in general and the REST protocol for communication. Additionally, users that should be able to call this web service, need the corresponding capabilities.

The plugin can be installed like any other Moodle plugin either through the Moodle interface or by placing the plugin manually in the Moodle data directory.
Once the plugin is installed it can be tested using Postman \cite{postman} or the Graasp Moodle App.

A detailed description of this process is given in the README.md file of the plugin which can be found in the appendix \ref{appendix:moodle_plugin_readme} or on GitLab.


\subsection{Event Descriptions} \label{sec:moodle_plugin_events}
The learning data returned by the plugin comes from the activity logs of a course in form of an array of events. Table \ref{table:ret_val_desc_get_course_data} describes a selection of values returned for each event. A complete list of data available can be found under \url{https://docs.moodle.org/dev/Events_API#Properties}.

\begin{table}[h]
\centering
\begin{tabularx}{\textwidth}{s y b m}
% Header
\cellcolor[HTML]{EEEEEE}\textbf{Key} & 
\cellcolor[HTML]{EEEEEE}\textbf{Type} & 
\cellcolor[HTML]{EEEEEE}\textbf{Description} &
\cellcolor[HTML]{EEEEEE}\textbf{Example Value} \\
\hline
% Content
action & String & The action being performed & viewed, assigned, created, \ldots \\
\hline
target & String & The target of the action & course, user, \ldots \\
\hline
edulevel & Int & The level of educational value of the event & 1 (teaching), \newline 2 (participating) \\
\hline
userid & Int & The user executing the action & 2, 73, 2994, \ldots \\
\hline
role* & String & The executing users assigned role in this course & student, teacher, \ldots \\
\hline
relateduserid & Int & The affected user if any & 2, 73, 2994, \ldots \\
\hline
timecreated & Int & The timestamp when the event occurred & 1592478840, 1584852734, \ldots \\
\hline
\end{tabularx}
% Caption
\caption{Return value description of \textit{get\_course\_data}} 
\label{table:ret_val_desc_get_course_data}
\end{table}

{\footnotesize * The key \textit{role} is added manually to the output and not included in the default activity log.}


\subsection{Anonymization}
As users learning data is exported, it must be ensured that no sensitive user information is exposed. The most practical solution would be an in-built anonymization function offered by Moodle that can be used, but such functionality does not exist. 
Hence, the information exported concerning the user is limited to his internal id. This reduces the scope of people able to identify a user to people with access to the Moodle instance with enough privileges to list all users. 

This approach is safe enough for the prototype and avoids a time-consuming task of creating our own anonymization function which prevents \gls{de-identification} entirely.

Furthermore, some \gls{la} require unanonymized user data. For instance, when follow up inquiries of students are pursued or other sources should be included such as paper exams. Keeping this in mind, data should be anonymizable after such a state upon request of the researcher.

\subsection{Code Review}
As defined in the development strategy, a code review is performed. André Nogueira from the Graasp team suggests the following changes:
\begin{itemize}
	\item Replace tabs with spaces to prevent misleading indentation caused by different tab sizes.
	\item Improve readability of code by separating code blocks with empty lines.
	\item Add supplementary comment to explain the usage of the non-intuitive code blocks.
	\item Access parameters only after validation.
\end{itemize}

All changes are implemented and are listed above only to be kept in mind for further development.

\subsection{Possible Improvements}
Since this is only a prototype, there is room for future work. The list below summarizes some starting points to continue the development of this Moodle plugin.

\begin{itemize}
	\item Refactor existing code to more tightly match the code guidelines from Moodle defined under \url{https://docs.moodle.org/dev/Coding_style}. For this, the Moodle specific \textit{code checker} plugins for IDEs could be used.
	\item Use \textit{recordsets} to iterate over database values instead of storing them in memory as explained under \url{https://docs.moodle.org/dev/Data_manipulation_AP}
	\item Create unit, acceptance and integration tests as recommended for plugin development by Moodle. 
	\item Offer multi-language support of the plugin.
	\item Add a configuration for the plugin. For example, allow a user to define the role for each course through an option, instead of the teacher role by default. This could be particularly interesting since the default Moodle roles can be overridden or even deleted.
	\item Publish the plugin on the official Moodle plugin repository.
\end{itemize}


\section{Graasp Moodle App}
The Graasp Moodle App is a frontend application that can be used within the \gls{ecosystem}. The app allows users to import data through the Moodle Web Service Plugin and offers a few additional interaction options. The source files are available on GitHub under \url{https://github.com/graasp/graasp-app-moodle}.


\subsection{Motivation}
The Moodle Web Service Plugin can be tested entirely using Postman. However, this is not convenient and makes it difficult to imagine future use cases through a dedicated Graasp Learning Analytics App. Hence, this app should visually walk a user through the configuration to request a token, let him select some courses and display the imported data in an understandable form. 

Additionally, possible interactions with the imported data are demonstrated. Concretely, the data can be filtered and saved as app instance resources. These functions will most likely be reused by a future Graasp Learning Analytics App.


\subsection{App Content}
\begin{figure}[h]
	\center
	\includegraphics[width=\textwidth, frame]{img/graasp-app/overview.png}
	\caption{Overview of the Graasp Moodle App with imported data}
	\label{fig:overview_app}
\end{figure}


As stated before, the motivation to make this app is to prove that data from an external \gls{lms} can be imported into Graasp. A visualization of the output of such an import is represented in Figure \ref{fig:overview_app}, which shows the main interface of the app.

In the following subsections, the different screens are presented and the possible actions described. 

\subsubsection{Connection Establishment}
The first action a user must do is to establishing a connection with Moodle. Thus, he enters the location of the Moodle instance he would like to connect to and valid user credentials in the settings window, as shown in Figure \ref{fig:connection_details_app}.

\begin{figure}[h]
	\center
	\includegraphics[width=.45\textwidth, fbox]{img/graasp-app/configure_connection.png}
	\caption{Configure the connection details}
	\label{fig:connection_details_app}
\end{figure}

Based on this information, an HTTPS request is sent to retrieve an access token from the Moodle instance. Once succeeded the user can  select the courses for which learning data should be imported.

\subsubsection{Course Selection}
The app makes an HTTPS request to search the Moodle instance for a list of available courses. The user can then select one or multiple courses from the previously fetched list as visible in Figure \ref{fig:course_selection_app}. Upon confirmation, the data is imported into the app.

\begin{figure}[h]
	\center
	\includegraphics[width=.45\textwidth, fbox]{img/graasp-app/select_course.png}
	\caption{Select the courses to import}
	\label{fig:course_selection_app}
\end{figure}


\subsubsection{Data Import}
Once the import has been started, another HTTPS request gets the data from the Moodle instance for the specified courses. The data is then stored in the applications state and displayed in a table as shown in Figure \ref{fig:table_imported_data_app}.

\begin{figure}[h]
	\center
	\includegraphics[width=\textwidth, frame]{img/graasp-app/imported_data.png}
	\caption{Table showing the imported data}
	\label{fig:table_imported_data_app}
\end{figure}

The data is represented in the table just as it is received, except for the \textit{timecreated} values. These values are converted to local date strings upon reception, as it is easier for humans to read them  in this format.


\subsubsection{Filter Data}
To present a possible use case to work with the imported data, a filter option is built-in. This allows the user to select the columns being displayed in the table via a multiselect input field visible in Figure \ref{fig:filter_columns}.

\begin{figure}[h]
	\center
	\includegraphics[width=.7\textwidth, fbox]{img/graasp-app/filter_columns.png}
	\caption{Filter the columns to display}
	\label{fig:filter_columns}
\end{figure}

Furthermore, data can be filtered by the values within a column using the multiselect input field presented in Figure \ref{fig:filter_values}. Those filter options are generated dynamically based on the content of the imported data and are available for every column. 

\begin{figure}[h]
	\center
	\includegraphics[width=.8\textwidth, fbox]{img/graasp-app/filter_values.png}
	\caption{Filter the rows to display}
	\label{fig:filter_values}
\end{figure}

The only exception is the \textit{timecreated} column, since the selection based on single values is not reasonable as shown in Figure \ref{fig:filter_timecreated}. A possible solution would be to generate a range slider only for this column. This would offer a better UX. Nonetheless, this was not considered pa priority, thus, it will only be listed in Section \ref{sec:graasp_app_improvements} as a possible improvement.

\begin{figure}[H]
	\center
	\includegraphics[width=.4\textwidth, fbox]{img/graasp-app/filter_timecreated.png}
	\caption{Filter the \textit{timecreated} column (not available)}
	\label{fig:filter_timecreated}
\end{figure}

\newpage

All of these filters are generated dynamically. To do this, there is a state variable called \textit{filters}, which holds for each column of the imported data an object with the attributes \textit{options} and \textit{selection}, as can be seen on Listing \ref{lst:filters_structure}.
Those are used to control the multiselect input field for each filter and perform the actual filtering when rendering the \textit{working data} table. To perform the filter for all columns, a \textit{for-loop} is used as shown on Listing \ref{lst:filter_rows}.



\begin{lstlisting}[language=JavaScript, caption={Filter structure with action as example}, label={lst:filters_structure}]
filters: {
	action: {
		options:   ["created", "viewed", "assigned", "submitted","..."],
		selection: ["viewed", "submitted"],
	}
	...
}
\end{lstlisting}

\begin{lstlisting}[language=JavaScript, caption={Filter the rows based on all possible filters}, label={lst:filter_rows}]
const availableColumns = ['userid', 'role', 'action', 'target', '...'];
const filteredData = data.filter(row => {
  return availableColumns.every(
    column =>
      filters[column].selection.length === 0 ||
      filters[column].selection.includes(row[column]),
  );
});
\end{lstlisting}


\subsubsection{Persist Data to Database}
Another implemented feature is the storage option. Imported and filtered data can be stored by clicking on the corresponding button shown in Figure \ref{fig:save_buttons}.

\begin{figure}[h]
	\center
	\includegraphics[width=.3\textwidth]{img/graasp-app/save_buttons.png}
	\caption{Save buttons to persist imported data to database}
	\label{fig:save_buttons}
\end{figure}

If a button is clicked, an app instance resource, corresponding to the object presented in Listing \ref{lst:app_instance_resource_object}, is stored in the database. The \textit{type} and \textit{visibility} are set to allow anyone to find the resources later easily. The \textit{dataSource} corresponds to the API endpoint used for this particular import. If the \textit{importedData} is filtered, the corresponding attribute \textit{filtered} is set to true as well.

\begin{lstlisting}[language=JavaScript, caption={App instance resource that is stored}, label={lst:app_instance_resource_object}]
{
  data: { importedData: dataToStore, source: dataSource, filtered: isDataFiltered },
  type: MOODLE_DATA,
  visibility: PUBLIC_VISIBILITY,
}
\end{lstlisting}

Once the data is stored, it is displayed in the list of \textit{stored resources} as shown in Figure \ref{fig:stored_resources}.

\begin{figure}[h]
	\center
	\includegraphics[width=.9\textwidth, fbox]{img/graasp-app/saved_resources.png}
	\caption{Stored resources from the database}
	\label{fig:stored_resources}
\end{figure}


\subsection{MoodleAPIRequests Object} \label{sec:moodle_api_requests_obj}
As mentioned earlier, the Graasp Moodle App exploits a \gls{Strategy Pattern} to handle the interaction between multiple source \glspl{lms}. Therefore, an abstract superclass \textit{Api Request} is used, from which each \textit{strategy} will inherit. A UML conform representation of the class is shown in Figure \ref{fig:class_diag_api_requests}.

\begin{figure}[h]
	\center
	\includegraphics[width=.45\textwidth]{img/diagrams/class_diag_api_requests.png}
	\caption{UML representation of abstract \textit{APIRequests} class}
	\label{fig:class_diag_api_requests}
\end{figure}

The \textit{strategy} for the interaction with Moodle is the \textit{MoodleAPIRequests} class and is used as example to describe its parameters and methods throughout the next four sections.

\subsubsection{Attributes}
The attributes for the strategies could be kept private since they should be modified only from within the strategy.  However, as JavaScript does not allow this without tedious workarounds, the current implementation allows the external modification of attributes.

Currently, there are two attributes which are set once a token is retrieved:
\begin{itemize}
	\item \textbf{lmsBaseUrl}: The location of the \gls{lms} used to send the requests to.
	\item \textbf{token}: The token used for authentication after the user has been authenticated once.
\end{itemize}

\subsubsection{Method: getToken()}
As usual when working with external systems, a certain authentication mechanism exist. For the \glspl{lms} TalentLMS \cite{talentlms_api}, Canvas \cite{canvas_api_doc} and Moodle, this is done by authenticating the user credentials once and then use the received token for all future calls. 

Listing \ref{lst:meth_sig_getToken} shows the method signature with all the parameters.

\newpage

\begin{lstlisting}[language=JavaScript, caption={Method signature getToken()}, label={lst:meth_sig_getToken}]
/**
 * Get a token for future authentication and store it locally.
 * @param lmsBaseUrl which should be used to send this and the following requests to. Will be stored locally
 * @param username used for authentication
 * @param password used for authentication
 * @returns {boolean} true if token has been saved and false when not.
 */
function async getToken(lmsBaseUrl, username, password) {}
\end{lstlisting}

\begin{bclogo}[
  couleur=bglightgrey,
  arrondi=0,
  logo=\bcinfo,
  barre=none,
  marge=10,
  ombre=true,
  noborder=true]{Storage of the \textit{lmsBaseUrl}}
To prevent sending a \textit{token} to the wrong \gls{lms}, the \textit{lmsBaseUrl} is stored like the object in the \textit{MoodleAPIRequests} object. Both values are then reused throughout the other methods. 
\end{bclogo}


\subsubsection{Method: getAvailableCourses()}
The Graasp Moodle App offers the user a list of courses from which he can import data. Hence, it must know which courses are available. Therefore, a list of available courses should be retrieved from the \gls{lms}. As this varies from system to system, this method is also part of the strategy. The method signature can be found in Listing \ref{lst:meth_sig_getAvailableCourses}.

\begin{lstlisting}[language=JavaScript, caption={Method signature getAvailableCourses()}, label={lst:meth_sig_getAvailableCourses}]
/**
 * Get a list of all available courses for this user.
 * @returns {*} a list of of available courses containing at least their ids
 */
function async getAvailableCourses() {}
\end{lstlisting}


\subsubsection{Method: getCourseData()}
The last interaction with the external \gls{lms} is the actual import of data. The returned data may have different content depending on the implementation. In the case of Moodle, it is a list of action log entries. The signature of the method is displayed in Listing \ref{lst:meth_sig_getCourseData}.

\begin{lstlisting}[language=JavaScript, caption={Method signature getCourseData()}, label={lst:meth_sig_getCourseData}]
/**
 * Load data for selected courses through API.
 * @param {(string|number)[]} selectedCoursesIds
 * @returns {*} the exported data
 */
function async getCourseData(selectedCoursesIds) {}
\end{lstlisting}


\subsection{Deployment Process}
There are four steps to deploy a Graasp App to the \gls{ecosystem}: 

\begin{enumerate}
	\item Create a release
	\item Setup the environment file
	\item Build the application 
	\item Deploy it on AWS
\end{enumerate}
	
Each of these steps is described further in detail in this section. At the end of step 4, the URL where the app is published will be displayed. This URL can be copied and used it to add a new app to a \gls{space}.

To follow this deployment process the following preconditions should be kept in mind:
\begin{itemize}
	\item The Graasp App has been created via the Graasp CLI. 
	\item A Graasp developer ID and an AWS access ID/key have been requested and obtained.
\end{itemize}

\subsubsection{Development and Production Environments}
Before deploying the application, it is important to know the difference between the development and production environments. As the name implies, the development environment should be used to test the app until it is ready for production. The development environment offers its own ecosystem under \url{https://dev.graasp.eu}, which requires a separate login. 

Once the app is deployed to production, the data is stored in the database of the Graasp production ecosystem. Changes later to the format of stored data may have unintended side effects for users already using the app. Therefore, newer versions of an app should be backwards compatible.

% Contains the explication to the three steps. In external file because of syntax highlighting issues with \lstinline
\input{31_graasp_app_deployment}

\subsection{Code Review}
The source code of the Graasp Moodle App is reviewed by Kim Phanhoang  from the Graasp team. She raises the following points to improve the source code:\\
{\footnotesize Items marked with * are not yet implemented.}
\begin{itemize}
	\item Use consequently translation strings based on react-i18next \cite{react_i18n}.
	\item Define a secondary color that is part of the Graasp color palette.
	\item *Factor the Material-UI theme \cite{materialui} out.
	\item *Factor out consequently constants such as view modes (teacher/student).
	\item *Move the functions that depend from component properties into the component instead of passing the properties object as argument.
\end{itemize}

Additionally, the user interface is reviewed by Juan Carlos Farah and Kim Phanhoang. As this project does not focus on the appearance of the Graasp Moodle App a complete list of deficits is omitted. However, it must be stressed that the interface must be optimized for smaller screens. Finally, the app is displayed in the Graasp configuration page for \glspl{app} and thus, has only limited space available.

\subsection{Possible Improvements} \label{sec:graasp_app_improvements}
During the development of the prototype, various interesting tracks are identified to improve the Graasp Moodle App. The list below resumes those improvements:
\begin{itemize}
	\item Support the import from different \glspl{lms}. This would increase the available learning data and imperatively augment the quality of the \gls{la}. As soon as multiple \glspl{lms} are included, the name could be changed to e.g. "Graasp Data Import App".
	\item \gls{la} are interesting for students, but currently the app is only available to teachers. Therefore, once it is possible to perform \gls{la}, a feature where students can import only their personal data could be interesting.
	\item By default, all data is imported from the \gls{lms}. It would be interesting to let the user select which columns or time periods should be imported. One of the benefits would be enhanced performance since the amount of data to import is reduced to what interests the user.
	\item No user interface is perfect - ever. There are numerous points that could be added to and optimized from button tooltips to object arrangement.
\end{itemize}


\section{Testing} \label{sec:general_testing}
There are no complex algorithms used throughout the Data Aggregation Module, but it is composed by two separated parts: the Graasp Moodle App and the Moodle Web Service Plugin. Therefore, the testing is focusing on system level instead of unit. Since tests are part of CI/CD, the setup and content of the tests is given in Section \ref{sec:codeship_pipeline}.

As additional test, the Moodle plugin has been successfully installed on the development server of Cyberlearn\footnote{Cyberlearn is the e-learning platform used by the HES-SO and has around active 15'000 users \cite{cyberlearn}}. There it passed the manual testing using Postman as described in the README.md file of the plugin which can be found in appendix \ref{appendix:moodle_plugin_readme}.


\section{Conclusion}
In the context of a dedicated \gls{la} application, the Data Aggregation Module offers a solid foundation for the import of learning data. Even though the solution currently supports only Moodle, the architecture is flexible and allows the integration of other \glspl{lms}.



