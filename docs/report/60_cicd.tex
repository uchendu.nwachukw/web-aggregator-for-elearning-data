\chapter{Objective 4: Automation}

\minitoc

Running tests and making a deployment are gratifying during the first release of an application. But what is with the second, third and thirtieth release? The excitement diminishes and these tasks are getting tedious. To prevent this situation, an objective of this project is the automation of testing and the deployment process.

In this context, the Graasp \gls{cli} is extended and a build pipeline setup with Codeship.

\section{Graasp CLI}
A \gls{cli} is offered to developers to facilitate the \gls{app} development. So far, only the \textit{new} command is available, which asks the developer what kind of setup he's looking for. At the end of the interrogation, a code template is downloaded from GitHub and different environment files are generated.

Upon mutual agreement between all involved parties of this project, the creation of a command to take care of the deployment was added to the task list. The following sections are giving an overview of the setup of the CLI and the recently added \textit{deploy} command.

\subsection{General Setup}
The \gls{cli} is implemented using Node.js \cite[node.js]. Instead of reinventing the wheel, the module \textit{yargs} \cite{yargs} is used. It takes care of: 
\begin{itemize}
	\item generating the command help;
	\item enabling auto-completion of commands; and
	\item parsing and type validation of command arguments.
\end{itemize}

The usage of Node.js allows the execution of bash commands on the host machine. Thus, there are no limits for the creation of \gls{cli} commands.

\subsection{The 'deploy' Command}
\glspl{app} have been deployed using an auxiliary bash script that:
\begin{itemize}
	\item validates parameters;
	\item checks environments variables;
	\item publishes files using the AWS CLI; and
	\item invalidates the cache for the published files.
\end{itemize}

The new \textit{deploy} command offers the exact same convenience, without the need of having the auxilliary bash script in the project folder. This offers the big advantage that changes to the deployment process have not to be done manually for each project, but simply the Graasp \gls{cli} can be updated.

To implement this new command, the above mentioned functionalities are rewritten in JavaScript and included in the CLI. The current pull request is still pending by the end of this project. Before he can be merged, the asynchronous executed tasks like uploading to AWS or loading environment variables.

Figure \ref{fig:output_deploy} shows the output in the console when deploying an application. The above mentioned issue is manifested in this capture by the duplication of the progress bar indicating the upload status.

\begin{figure}[h]
	\center
	\includegraphics[width=\textwidth]{img/cli/deploy_cmd_output.png}
	\caption{Console output of a deployment using the \textit{deploy} command}
	\label{fig:output_deploy}
\end{figure}


\subsubsection{Flags}
To provide a consistent experience for the developers that are used to the auxillary script, the flags are reused with one minor change. Since the \textit{--version} flag is used by default by \textit{yargs}to print out the version of the \gls{cli}, it is replaced by a \textit{--tag} flag. Even though it should be possible to overwrite such a default flag, no simple solution seems to exist. Further research concerning this issue are abandoned due to low priority. 

Figure \ref{fig:deploy_help} shows the output when calling the command with the \textit{--help} flag.


\begin{figure}[H]
	\center
	\includegraphics[width=.8\textwidth]{img/cli/deploy_cmd_help.png}
	\caption{Console output for the help of the \textit{deploy} command}
	\label{fig:deploy_help}
\end{figure}

  
\subsubsection{AWS SDK for JavaScript}
The auxilliary script published the files and invalidated the cache through the AWS \gls{cli}. This required a prior manual installation of the \gls{cli}. With the new \textit{deploy} command this is no longer necessary. Instead of executing the commands \textit{aws s3 sync} and \textit{aws cloudfront create-invalidation} on the machine, the official \textit{aws-sdk} node module is used. 

The SDK offers a wide range of functions to interact with AWS. However, a \textit{sync()}\footnote{Syncing in this context means the upload from the local source directory to AWS and removing obsolete files on AWS.} function does not exist. Therefore, a complementary node module \textit{s3-node-client} is used to fill the gap. The code performing the sync operation by uploading an entire directory using this client is displayed in listing \ref{lst:s3_sync}. 
The alternative would be the recursive traversal of the source directory and then upload each file individually using the \textit{upload()} function of the \textit{aws-sdk}. However, this appraoch would result in more self-written code and therefore comes with the risk of introducing errors.

\begin{lstlisting}[language=JavaScript, caption={The re-implementation of sync() using the s3-node-client}, label={lst:s3_sync}]
const params = {
  localDir: build,
  deleteRemoved: true, // default false, whether to remove s3 objects
  // that have no corresponding local file.

  s3Params: {
    Bucket: BUCKET,
    Prefix: APP_PATH,
    // other options supported by putObject, except Body and ContentLength
    // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html
  },
};
const uploader = client.uploadDir(params);
uploader.on('error', function (err) {
  console.error('unable to sync:', err.stack);
});
uploader.on('progress', function () {
  console.log('progress', uploader.progressAmount, uploader.progressTotal);
});
uploader.on('end', function () {
  console.log('done uploading');
});
\end{lstlisting}

\subsection{Other Improvements}
Along the implementation of the \textit{deploy} command, some other improvements to the Graasp \gls{cli} are done. A short description of those improvements is given in the list below:
\begin{itemize}
	\item Add  the prettier configuration to the project.
	\item Update the ESLint configuration to respect the \textit{prettier} rules as it is usually the case for Graasp projects and turn off the rule which warns from \textit{console statements} in the code. To warn from the latter is not reasonable since the \gls{cli} uses console statements to inform the user about its progress or problems.
	\item Create a utility class and factor out the \textit{spawnProcess()} or \textit{promisify()} function that are very likely to be shared among different modules.
\end{itemize}
 
 
\section{Codeship Build Pipeline} \label{sec:codeship_pipeline}
The \gls{ecosystem} relies for automation tasks on Codeship. Thus, Codeship is used to set up a build pipeline where tests are executed and code is deployed to AWS. The pipeline is triggered on every commit to the GitHub repository of the Graasp Moodle App. Figure \ref{fig:pipeline} represents the different steps of the pipeline.
The functioning of the different parts of this pipeline is described in the following sections.

\begin{figure}[H]
	\center
	\includegraphics[width=.5\textwidth]{img/diagrams/pipeline.png}
	\caption{Visualization of the pipeline}
	\label{fig:pipeline}
\end{figure}


\subsection{Fundamentals}
To better understand the build environment and its configuration, an introduction to the key concepts of Codeship is given throughout the next subsections.

\subsubsection{Build Environment}
Codeship uses Docker to set up a build environment. Thus, the containers from the docker environment described in Section \ref{sec:docker_env} are reused.

These containers allow running tests in them and create application builds that can then be deployed to the AWS. They are declared as services in the \textit{codeship-services.yml} file, which has a similar structure to \textit{docker-compose}. Listing \ref{lst:codeship_service} shows the declaration of the Graasp Moodle App container.


\begin{lstlisting}[caption={Declaration of the Graasp Moodle App service}, label={lst:codeship_service}]
app:
  build:
    dockerfile: Dockerfile
  encrypted_env_file: .env.test.encrypted
\end{lstlisting}

\newpage

\subsubsection{Step Configuration}
The content of a build is divided into and defined by multiple steps using the \textit{codeship-steps.yml} file.  Therefore, a step determines:
\begin{itemize}
	\item when it should be run (each commit, for certain branches or on specific tags);
	\item which service is used during this step; and
	\item which command should be executed on the given service(s).
\end{itemize}

The definition of the step that runs the tests is shown in Listing \ref{lst:codeship_step}.

\begin{lstlisting}[caption={Declaration of the testing step}, label={lst:codeship_step}]
- name: default
  type: serial
  steps:
    - name: lint
      service: app
      command: yarn lint
    - name: test
      service: app
      command: yarn test
    - name: integration-test
      service: data-aggregation-module
      command: yarn test:integration
\end{lstlisting}

\subsubsection{Environment Variable Encryption and Decryption}
The deployment to AWS requires authentication. If this process is automated, some credentials must be passed to the pipeline. This is usually done by defining them in environment variables which can be loaded from a \textit{.env} file. 

To prevent exposing sensitive data in the GitHub repository, Codeship allows passing an AES-256 bit encrypted \cite{aes256} \textit{.env} file. Advanced Encryption Standard (AES) uses the same key for encryption and decryption. 

\begin{bclogo}[
	couleur=bglightgrey,
	arrondi=0,
	logo=\bcinfo,
	barre=none,
	marge=10,
	ombre=true,
	noborder=true]{Be Careful of Sharing Unwillingly Encrypted Sensitive Data}
As the encrypted file is pushed to the GitHub repository, colleagues may have access to it. If those have as well the AES key used for encryption, they can decrypt the file and gain access to sensitive data. 
To reduce this risk, it is for example recommended to use project specific credentials with minimal required privileges for deployment instead of developer related ones.
\end{bclogo}

\newpage

\subsection{Tests} \label{sec:test_strat}
Every time when code is committed to the GitHub repository, three tools are started:
\begin{enumerate}
	\item ESLint for JavaScript code analysis
	\item Jest for component testing
	\item Cypress \cite{cypress} for integration testing
\end{enumerate}

The focus is put on integration testing because of the nature of the Data Aggregation Module. It does not rely on complex algorithms and is composed of two separate parts: the Moodle Web Service Pugin and the Graasp Moodle App.

The integration test is executed using Cypress. It validates the behavior of a web application by simulating user interactions in a browser. Therefore, the test is performed on the Graasp Moodle App and following the scenario presented in Figure \ref{fig:cypress_scenario}.

\begin{figure}[h]
	\center
	\includegraphics[width=.7\textwidth]{img/cypress_test_result.png}
	\caption{Cypress integration test scenario and test run with duration in seconds}
	\label{fig:cypress_scenario}
\end{figure}

It is worth pointing out, that the test is written with the goal of being self-explaining. Thus, instead of writing a single test and commenting the different parts, the test is decoupled into sections and each section title gives enough information to assume its content.

\newpage

\subsection{Deployments}
On every push to the \textit{master} branch of the GitHub repository, the code is tested as usual and in case of successful tests, the Graasp Moodle App is deployed. Hence, the pipeline consists of three steps: 
\begin{enumerate}
	\item Pass the tests described in Section \ref{sec:test_strat}.
	\item Create a build for the development environment of the application through a dedicated service and store the \textit{build/} folder in a \textit{volume}\footnote{Codeship uses similar to Docker \textit{volumes} to persist and share data between different steps.}.
	\item Deploy the built application via an offered deployment service from Codeship to AWS.
\end{enumerate}

By following the development practices for \glspl{app}, a new version tag indicates a new release. When a tag with a valid version number is pushed to the GitHub repository, a fourth pipeline step is triggered: the deployment to production. To perform this step, a manual approval is required after the other three steps have been executed. Once approved, steps two and three are repeated for and to the production environment.



\section{Possible Improvements}
Automation has almost no limits. As a consequence, there could be many more functionalities added to the current setup. The following list contains a few suggestions for the build pipeline and the Graasp \gls{cli}:
\begin{itemize}
	\item Extend the CLI with commands for:
	\begin{itemize}
		\item \textit{.env} file encryption, which is currently done using a \textit{Makefile};
		\item renaming a Graasp App; and
		\item creating a new release.
	\end{itemize}
	\item Optimize the speed of builds in the pipeline.
	\item Store the environment variables used during the build pipeline in a dedicated \textit{.env.test} file.
\end{itemize}

\section{Conclusion}
The explored automation options show that tedious and repetitive tasks can be delegated from the developer to the machine. This demands an initial effort, but the increased efficiency afterwards justifies this investment.

The implemented mechanisms demonstrate a wide range of applications for automation. Additionally, the foundation of the pipeline, the created services, can be reused by new steps.
