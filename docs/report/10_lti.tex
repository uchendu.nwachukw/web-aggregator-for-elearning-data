\chapter{Objective 3: Embed a Graasp App into a Moodle Course}

\minitoc

A common problem in \gls{la} is not enough data. Thus, an objective of this project is the generation of richer learning data for Moodle by embedding a \gls{app} into a course to gain better and more reliable insights when performing \gls{la}.

Based on this motivation, it is the responsibility of the \gls{app} to generate learning data. Until now, this feature is rarely employed as there was no use case. But with the aim at performing \gls{la}, this must change and the app developers are mandated to log the actions of the students more detailed.

In the following sections, the standard \gls{lti} is explored and a possible implementation to embed a \gls{app} in Moodle using LTI is elaborated. Simultaneously, alternative standards and implementations are presented in relation to the motivation of generating more learning data.


\section{Analysis LTI}
The \gls{ims} published the first version of the \gls{lti} standard in 2010. The objective of the standard is the establishment of a secured connection between a \gls{tp} and a \gls{tc} by profiting of modern security techniques. The connection is used to exchange data about the user, his privileges and his course activities.

Meanwhile, many modern \gls{lms}, such as Moodle, Blackboard or D2L Brightspace \cite{wikipedia_lti_2020}, adopted the \gls{lti} to communicate with third party activities.

For the analysis of the standard and elaboration of implementation approaches, the \gls{lti} Core Specification (Version 1.3) is used as reference \cite{lti_spec_1_3}.

\subsection{Typical Use Case} \label{sec:lti_use_case}
Let's say a teacher uses the \gls{lms} Moodle. Now, he would like to make a coding assessment for python for his students within his "Python Beginner's Course". However, Moodle does not offer such an activity, neither could he find a Moodle plugin that satisfies his desire.
He decides to develop a small online code editor to write and execute python code. It works perfectly fine, but how does he know which student has written which code and assign them a corresponding grade?
That's when \gls{lti} comes in handy. By converting his online code editor into an \gls{lti} \gls{tp}, he benefits from the three following functionalities:
\begin{enumerate}
	\item Online code editor accessible as an activity in Moodle.
	\item Secure transfer of user related data from Moodle to the online code editor.
	\item Grade assignment in the online code editor and transmission to Moodle.
\end{enumerate}

Figure \ref{fig:lti_functionalities} illustrates these three functionalities and associates them in the context of \gls{tc} and \gls{tp}.

\begin{figure}[h]
	\center
	\includegraphics[width=\textwidth]{img/lti_collage/lti_functionalities.png}
	\caption{LTI Functionalities shown between Moodle (left) and Graasp Code App (right)}
	\label{fig:lti_functionalities}
\end{figure}


\subsection{LTI Communication Flow} \label{sec:lti_comm_flow}
The communication between \gls{tc} and \gls{tp} defined by \gls{lti} may be separated into three phases:
\begin{enumerate}
	\item Registration of \gls{tp} in \gls{tc}.
	\item Launch \gls{tp} through \gls{tc}.
	\item Transmission of grades from \gls{tp} to \gls{tc}.
\end{enumerate}

Take note, that the schemes in the following subsections are all \gls{lti} compatible. However, they should rather be interpreted as possible workflows to offer the service standardized by \gls{lti}, which by definition specifies the payloads of particular messages exchanged between \gls{tc} and \gls{tp}. The \gls{lti} specified messages are highlighted red therefore in the following schemes.

\subsubsection{Registration of Tool Provider in Tool Consumer}
Before a \gls{tp} can be used by a \gls{tc}, a registration is completed, as represented in Figure \ref{fig:lti_registration_flow}, where meta information and a public key are exchanged to secure the communication later.

\begin{figure}[h]
	\center
	\includegraphics[width=12cm]{img/diagrams/schema_lti_registration_flow.png}
	\caption{LTI registrate \gls{tp} in \gls{tc}}
	\label{fig:lti_registration_flow}
\end{figure}

\subsubsection{Launch Tool Provider through Tool Consumer}
One of the remarkable advantages of \gls{lti} is the launch of a \gls{tp} through a \gls{tc}. Figure \ref{fig:lti_launch_flow} shows which actions and messages are necessary to securely perform such a launch \cite{sandiegocodeschool_2019}.

\begin{figure}[h]
	\center
	\includegraphics[width=12cm]{img/diagrams/schema_lti_launch_flow.png}
	\caption{LTI launch \gls{tp} from withing \gls{tc}}
	\label{fig:lti_launch_flow}
\end{figure}

The authentication is done using the OpenID Connection (OIDC) Login Flow (2), where the \gls{tc} serves as \gls{OpenID} provider. This is done by exchanging \gls{OpenID} tokens which prove the user's identity to the \gls{tp}.

The launch request (5) includes a unique identifier for the user and his role in the current course in the \gls{tc} (e.g. instructor/teacher or student). Supplementary, information such as the user's name and email may optionally be transferred. If necessary, custom fields can be added that contain other information that will be used by the \gls{tp}.

\subsubsection{Transmission of Grades from Tool Provider to Tool Consumer}
Once the student has finished the activity, he may receive a grade which then can be retransmitted from the \gls{tp} to the \gls{tc}. Thus, the \gls{tc} must allow grade assignments through the \gls{tp}. \gls{lti} imposes therefore the payload of a specific message as represented by figure \ref{fig:lti_grade_transmission_flow}.

\begin{figure}[h]
	\center
	\includegraphics[width=12cm]{img/diagrams/schema_lti_submit_activity_and_grade.png}
	\caption{LTI transmit grade from \gls{tp} to \gls{tc}}
	\label{fig:lti_grade_transmission_flow}
\end{figure}

The assignment of the grade does not have to be made automatically. It is also possible, that a teacher assigns the grade manually in the \gls{tp}. Thereafter, the grade can be submitted as described in figure \ref{fig:lti_grade_transmission_flow} to the \gls{tc}.

\begin{bclogo}[
	couleur=bglightgrey,
	arrondi=0,
	logo=\bcinfo,
	barre=none,
	marge=10,
	ombre=true,
	noborder=true]{Required Privileges}
The teacher assigning the grades in the \gls{tp} \textbf{must} posses enough privileges in the \gls{tc} to assign grades.
\end{bclogo}


\section{Alternatives to LTI} 
There is no alternative that brings the exact same functionalities as \gls{lti} with it. However, looking at the motivation to generate more learning data, \gls{scorm} and \gls{xapi} are promising concepts. The following sections are giving a quick overview of the two standards and highlight the relation with the motivation behind the investigation of \gls{lti} in the scope of this project.

\subsection{SCORM}
With the latest revision in 2009 some time has passed, but \gls{scorm}\cite{scorm_com} is still widely used and supported. It allows creating and integrating learning content in all \glspl{lms} that support the standard. More precisely, it specifies that learning content should:
\begin{itemize}
	\item be packaged in a ZIP file;  
	\item use an XML file to describe its content;
    \item rely on JavaScript to communicate; and
    \item define the content sequence using rules in XML.
\end{itemize}

\gls{scorm} is a standard that defines the interaction between learning content and an \gls{lms} just like \gls{lti}, but there is one major difference: the learning contents source files are uploaded to the \gls{lms}. 

Putting \gls{scorm} into the context of this project, the consequence would be that a \gls{app} either has to be configured directly in the source code or an \textit{export to \gls{scorm}} function must be developed and made available in the teacher mode of Graasp. 
In both cases, the base code of the \glspl{app} needs to be adapted to implement the standard and exploit it to transmit the users actions to the \gls{lms}.

An interesting feature of \gls{scorm} is, that it grants to record the duration for a student to answer questions in a quiz. Being limited to measure the interaction within a quiz, \gls{scorm} might offer some additional insights but is not the hoped-for breakthrough.

\subsection{xAPI}
The acknowledgment that learning happens everywhere sparked the urge for a new standard to collect learning data from different sources and store it centralized. \gls{xapi} defines an entry (statement) to be stored over three attributes: \textit{actor}, \textit{verb} and \textit{object}. Which allows storing events such as "John Doe submitted an assignment".

The goal of storing learning data centrally in a learning resource storage (LRS) is to have a single point of access for complete dashboards, reports and \gls{la} as can be seen in Figure \ref{fig:xapi_use_case}.

\begin{figure}[h]
	\center
	\includegraphics[width=.9\textwidth]{img/xapi/schema_xapi_use_case.png}
	\caption{General idea behind xAPI}
	\label{fig:xapi_use_case}
\end{figure}

In the context of this project, \gls{xapi} could be used to report events back from the \gls{app} to the \gls{lms}. This can be achieved under two circumstances:
\begin{enumerate}
	\item The meta information about the context (user and course information) in the \gls{lms} must be transmitted to the embedded \gls{app} to formulate later the \gls{xapi} statements (e.g. via query parameters).
	\item The \gls{lms} must offer an interface supporting xAPI to report from the \gls{app} (e.g. via a REST API).
\end{enumerate}	

\begin{figure}[H]
	\center
	\includegraphics[height=\schemagraaspheight]{img/xapi/schema_xapi_architecture.png}
	\caption{Possible architecture when using xAPI}
	\label{fig:xapi_archi}
\end{figure}

Figure \ref{fig:xapi_archi} visualizes an architecture where the \gls{app} is deployed as standalone app\footnote{The \gls{app} may as well be deployed in the \gls{ecosystem}. The advantages and disadvantages presented in Section \ref{sec:deploy_in_graasp} and \ref{sec:deploy_standalone} discussing the deployment options should be considered when pursing an approach using \gls{xapi} further.} and without specifying the protocols that the two systems use to interact, since \gls{xapi} specifies only the format to store events. This is something that needs further investigation to find an optimal and generic implementation to support multiple \glspl{lms}. Still, \gls{xapi} seems like an alternative worth having a deeper look at to increase the available learning data for \gls{la}.


\section{Embedding With LTI}
A possible way to embed a \gls{app} in a Moodle course is using \gls{lti}. In that case, the \gls{app} is converted to a \gls{tp} and then embedded in Moodle that serves as \gls{tc}. By making the app LTI compatible, it can be used without further adaption in any \gls{lms} supporting LTI. To achieve such compatibility, either the \gls{app} itself or the \gls{space} must be handling the requests defined by LTI, depending on how the \gls{app} is deployed.

\glspl{app} are usually deployed within the \gls{ecosystem} and then made available through \glspl{space}. Alternatively, they can be deployed as standalone application, which implies that their functionality is limited as they can't use the Graasp API. Even though there are limitations, both deployment variants are compatible with the motivation of generating rich learning data, but it is worth pointing out their advantages and disadvantages.

\subsection{Deployed within Graasp Ecosystem} \label{sec:deploy_in_graasp}
When the \gls{app} is deployed within the \gls{ecosystem}, the \gls{lti} related message exchange takes place between the LTI Activity and the \gls{space} where the target App Instance is located. Such an architecture is represented in Figure \ref{fig:lti_deploy_graasp_ecosystem} and Table \ref{table:adv_disadv_deploy_ecosystem} lists the associated advantages and disadvantages.

\begin{figure}[h]
	\center
	\includegraphics[height=\schemagraaspheight]{img/lti_implementation/schema_lti_deployed_graasp.png}
	\caption{Deployment of the Graasp App within the \gls{ecosystem}}
	\label{fig:lti_deploy_graasp_ecosystem}
\end{figure}

\begin{table}[h]
\centering
\begin{tabularx}{\textwidth}{m | m}
% Header
\cellcolor[HTML]{EEEEEE}\textbf{Advantages} & 
\cellcolor[HTML]{EEEEEE}\textbf{Disadvantages} \\
\hline
% Content
+ Since the adaptions are in the \gls{ecosystem}, they are automatically making all \glspl{app} \gls{lti} compatible.
\newline +  Exploit the \gls{lti} standard by exchanging user information.
\newline + Can store learning data directly in the Graasp Database.
\newline + Profit from the Graasp API. 
&
- Requires a profound understanding of the \gls{ecosystem}.
\\
\hline
\end{tabularx}
% Caption
\caption{Advantages and disadvantages when embedding a Graasp App that is deployed within the \gls{ecosystem}} 
\label{table:adv_disadv_deploy_ecosystem}
\end{table}


\subsection{Deployed as Standalone App} \label{sec:deploy_standalone}
If the \gls{app} is deployed as standalone application, the \gls{lti} communication is between the \gls{lti} Activity and the \gls{app}. Figure \ref{fig:lti_deploy_standalone} represents such an architecture. The advantages and disadvantages are listed at the end of this section in table \ref{table:adv_disadv_deploy_standalone}.

\begin{figure}[H]
	\center
	\includegraphics[height=\schemagraaspheight]{img/lti_implementation/schema_lti_deployed_standalone.png}
	\caption{Deployment as a standalone App}
	\label{fig:lti_deploy_standalone}
\end{figure}

It is worth mentioning, that this deployment option is investigated because of an initial misinterpretation of the \gls{lti} standard. It was assumed, that \gls{lti} can be used to report learning data back to the \gls{tc} in addition to the features presented in \ref{sec:lti_comm_flow}. Since this is not the case, the learning data generated by the \gls{app} has to be reported back to the external \gls{lms} using another mechanism. How this mechanism would look like depends on the external \gls{lms}. 

For example when the external \gls{lms} is Moodle, a plugin could be created that exposes a function to report events to. The plugin then handles those events and stores it in the intern database.
It becomes obvious, that already for an Open Source \gls{lms} supplementary tweaks are necessary. How this could be done for a commercially distributed \gls{lms} depends from to many factors to be estimable.

\begin{table}[h]
\centering
\begin{tabularx}{\textwidth}{m | m}
% Header
\cellcolor[HTML]{EEEEEE}\textbf{Advantages} & 
\cellcolor[HTML]{EEEEEE}\textbf{Disadvantages} \\
\hline
% Content
+  Easy to understand shared codebase between \glspl{app}.
\newline + Learning data from a course is stored in a single place (the database of the external \gls{lms}).
&
- Depends on a reporting mechanism in the external \gls{lms}.
\newline - No database to store user sessions from the \gls{app}.
\\
\hline
\end{tabularx}
% Caption
\caption{Advantages and disadvantages when embedding a Graasp App that is deployed as standalone app} 
\label{table:adv_disadv_deploy_standalone}
\end{table}


\subsection{Conclusion}
While the project advanced, the conclusion has been drawn that a prototype for the implementation of \gls{lti} can not be done in a satisfying form. 

The deployment within the \gls{ecosystem} results in changes deep inside the architecture of the \gls{ecosystem}. Consequently, this option is not chosen, even if it is the client's preferred approach.

In contrast, when deployed as standalone app, a reporting mechanism specific to an external \gls{lms} must be developed. This does not reflect the vision of the client of a generic solution that supports different \glspl{lms}. As a consequence, this is neither an option.

Since no other deployment option is available, no prototype is realized. However, an alternative way of making a Graasp App available within an external \gls{lms} is elaborated in the next section.


\section{Embedding Without LTI} \label{sec:embed_app_without_lti}
As a prototype for a \gls{app} implementing \gls{lti} is not realizable in the frame of this project, a simplified form to include a \gls{app} in an external \gls{lms} is elaborated. 

To do so, the \gls{app} is deployed within the \gls{ecosystem} and included in a public \gls{space}. This allows to access the specific Graasp App directly using an URL like \url{https://viewer.graasp.eu/en/pages/<spaceId>/subpages/<subspaceId>/resources/<resourceId>}. Where \textit{resourceid} references an instance of a \gls{app}.

Most \glspl{lms} allow to embed such an URL within an \textit{iframe} in a course. For example Figure \ref{fig:embed_app_in_mdl} shows the embedding of the Graasp Piano App in a Moodle course.

\begin{figure}[h]
	\center
	\includegraphics[frame, width=\textwidth]{img/embed_graasp_app_in_mdl.png}
	\caption{Embedded Graasp App in Moodle}
	\label{fig:embed_app_in_mdl}
\end{figure}

Even though this makes a \gls{app} available in an \gls{lms}, it comes with some downsides:
\begin{enumerate}
	\item It works only for \glspl{app} in public \glspl{space}. 
	\item No link between the learning data generated in the \gls{app} and the final course outcome in the \gls{lms} can be made because no data is shared.
	\item Some requests are blocked from within the iframe which may break some \glspl{app}(e.g. Code).
\end{enumerate}


\section{Conclusion}
Throughout this chapter the standard \gls{lti} has been explored and associated implementation approaches elaborated for the creation of richer learning data. Even though no prototype using LTI has been realized, the explication of the message exchange between \gls{tc} and \gls{tp} will be helpful in the case where Graasp is made \gls{lti} compatible.

After all, the alternative for embedding a \gls{app} in an \gls{lms} through an iframe is easy to put in place and requires only minor modifications in existing \glspl{app}. The \gls{la} performed on the newly generated learning data will define the value of this solution. 



