\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}
\contentsline {chapter}{\numberline {2}Objective 1: Learning Analytics}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Goals}{8}{section.2.1}
\contentsline {section}{\numberline {2.2}Collected Data}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Ethical Concerns}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Confidentiality Aspects}{9}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Privacy Issues}{9}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Data Anonymization}{10}{section.2.4}
\contentsline {section}{\numberline {2.5}Conclusion}{10}{section.2.5}
\contentsline {chapter}{\numberline {3}Objective 2: Data Aggregation Module}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Architecture}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Development Strategy}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Docker Environment}{13}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Docker Container: moodle-db}{14}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Docker Container: moodle-web-server}{14}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Docker Container: graasp-moodle-app}{15}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Limitations}{15}{subsection.3.3.4}
\contentsline {section}{\numberline {3.4}Moodle Web Service Plugin}{15}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Motivation}{15}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Plugin Content}{16}{subsection.3.4.2}
\contentsline {subsubsection}{db/services.php}{16}{subsection.3.4.2}
\contentsline {subsubsection}{externallib.php}{16}{lstnumber.3.2.6}
\contentsline {subsubsection}{version.php}{18}{lstnumber.3.7.10}
\contentsline {subsection}{\numberline {3.4.3}Installation}{19}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Event Descriptions}{19}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Anonymization}{19}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Code Review}{20}{subsection.3.4.6}
\contentsline {subsection}{\numberline {3.4.7}Possible Improvements}{20}{subsection.3.4.7}
\contentsline {section}{\numberline {3.5}Graasp Moodle App}{20}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Motivation}{21}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}App Content}{21}{subsection.3.5.2}
\contentsline {subsubsection}{Connection Establishment}{21}{figure.caption.7}
\contentsline {subsubsection}{Course Selection}{22}{figure.caption.8}
\contentsline {subsubsection}{Data Import}{22}{figure.caption.9}
\contentsline {subsubsection}{Filter Data}{23}{figure.caption.10}
\contentsline {subsubsection}{Persist Data to Database}{24}{lstnumber.3.10.8}
\contentsline {subsection}{\numberline {3.5.3}MoodleAPIRequests Object}{25}{subsection.3.5.3}
\contentsline {subsubsection}{Attributes}{25}{figure.caption.16}
\contentsline {subsubsection}{Method: getToken()}{25}{figure.caption.16}
\contentsline {subsubsection}{Method: getAvailableCourses()}{26}{bclogocompteur.3}
\contentsline {subsubsection}{Method: getCourseData()}{26}{lstnumber.3.13.5}
\contentsline {subsection}{\numberline {3.5.4}Deployment Process}{27}{subsection.3.5.4}
\contentsline {subsubsection}{Development and Production Environments}{27}{Item.11}
\contentsline {subsubsection}{Step 1: Create a Release}{27}{Item.11}
\contentsline {subsubsection}{Step 2: Setup for Build and Deployment}{27}{Item.15}
\contentsline {subsubsection}{Step 3: Build the Application}{28}{lstnumber.3.15.12}
\contentsline {subsubsection}{Step 4: Deploy on AWS}{28}{lstnumber.3.15.12}
\contentsline {subsection}{\numberline {3.5.5}Code Review}{28}{subsection.3.5.5}
\contentsline {subsection}{\numberline {3.5.6}Possible Improvements}{29}{subsection.3.5.6}
\contentsline {section}{\numberline {3.6}Testing}{29}{section.3.6}
\contentsline {section}{\numberline {3.7}Conclusion}{29}{section.3.7}
\contentsline {chapter}{\numberline {4}Objective 3: Embed a Graasp App into a Moodle Course}{30}{chapter.4}
\contentsline {section}{\numberline {4.1}Analysis LTI}{30}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Typical Use Case}{31}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}LTI Communication Flow}{31}{subsection.4.1.2}
\contentsline {subsubsection}{Registration of Tool Provider in Tool Consumer}{31}{Item.25}
\contentsline {subsubsection}{Launch Tool Provider through Tool Consumer}{32}{figure.caption.18}
\contentsline {subsubsection}{Transmission of Grades from Tool Provider to Tool Consumer}{33}{figure.caption.19}
\contentsline {section}{\numberline {4.2}Alternatives to LTI}{33}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}SCORM}{33}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}xAPI}{34}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Embedding With LTI}{35}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Deployed within Graasp Ecosystem}{35}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Deployed as Standalone App}{36}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Conclusion}{37}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Embedding Without LTI}{37}{section.4.4}
\contentsline {section}{\numberline {4.5}Conclusion}{38}{section.4.5}
\contentsline {chapter}{\numberline {5}Objective 4: Automation}{39}{chapter.5}
\contentsline {section}{\numberline {5.1}Graasp CLI}{39}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}General Setup}{39}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}The 'deploy' Command}{40}{subsection.5.1.2}
\contentsline {subsubsection}{Flags}{40}{figure.caption.28}
\contentsline {subsubsection}{AWS SDK for JavaScript}{41}{figure.caption.29}
\contentsline {subsection}{\numberline {5.1.3}Other Improvements}{42}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Codeship Build Pipeline}{42}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Fundamentals}{42}{subsection.5.2.1}
\contentsline {subsubsection}{Build Environment}{42}{subsection.5.2.1}
\contentsline {subsubsection}{Step Configuration}{43}{lstnumber.5.2.4}
\contentsline {subsubsection}{Environment Variable Encryption and Decryption}{43}{lstnumber.5.3.12}
\contentsline {subsection}{\numberline {5.2.2}Tests}{44}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Deployments}{45}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Possible Improvements}{45}{section.5.3}
\contentsline {section}{\numberline {5.4}Conclusion}{45}{section.5.4}
\contentsline {chapter}{\numberline {6}Conclusion}{46}{chapter.6}
\contentsline {section}{\numberline {6.1}Future Work}{46}{section.6.1}
\contentsline {section}{\numberline {6.2}Personal Conclusion}{46}{section.6.2}
\contentsline {chapter}{\numberline {A}Project Specification}{47}{appendix.A}
\contentsline {section}{\numberline {A.1}Context}{47}{section.A.1}
\contentsline {section}{\numberline {A.2}Objectives}{47}{section.A.2}
\contentsline {section}{\numberline {A.3}Activities}{48}{section.A.3}
\contentsline {subsection}{\numberline {A.3.1}Planning}{48}{subsection.A.3.1}
\contentsline {subsection}{\numberline {A.3.2}Milestones}{49}{subsection.A.3.2}
\contentsline {chapter}{\numberline {B}Test-Driven Development}{50}{appendix.B}
\contentsline {chapter}{\numberline {C}Readme: WAFED Moodle Webs Service plugin for Moodle 3.X}{51}{appendix.C}
\contentsline {section}{\numberline {C.1}Configure Your Moodle Instance}{51}{section.C.1}
\contentsline {section}{\numberline {C.2}Install Plugin}{52}{section.C.2}
\contentsline {subsection}{\numberline {C.2.1}Installing Manually at the Server (recommended)}{52}{subsection.C.2.1}
\contentsline {subsection}{\numberline {C.2.2}Installing via Uploaded ZIP File}{52}{subsection.C.2.2}
\contentsline {section}{\numberline {C.3}Test the Plugin with Postman (optional)}{52}{section.C.3}
\contentsline {subsection}{\numberline {C.3.1}Request a Token}{52}{subsection.C.3.1}
\contentsline {subsection}{\numberline {C.3.2}Get a List of Available Courses}{53}{subsection.C.3.2}
\contentsline {subsection}{\numberline {C.3.3}Get the Activity Log for a specific Course}{53}{subsection.C.3.3}
\contentsline {section}{\numberline {C.4}You're Done! Use it with the Graasp Learning Analytics App}{54}{section.C.4}
\contentsline {chapter}{Glossary}{55}{section*.36}
