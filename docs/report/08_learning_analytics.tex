\chapter{Objective 1: Learning Analytics}

\minitoc

With the growing integration of online courses into academic studies, more and more data is collected about the interaction between students and online resources. This process is recently even amplified by the COVID-19 pandemic. By applying \gls{la} on this data, insights can be gained into the learning process of students and possible correlation between specific behavior patterns and the final course grade. 

\gls{la} is not restricted to data in hindsight of online courses. Institutions like the University of Greenwich include attendance statistics into their analysis \cite{university_of_greenwich}. Others are looking at number of items borrowed from a library or the usage of dedicated learning areas \cite{CRL16603}. 
However, as per the specification of this project, the following sections are only looking at \gls{la} on data collected while interacting with an \gls{lms}.

The purpose of this section is the analysis of data confidentiality aspects in the context of \gls{la}. To preform and understand this analysis, the appliance of \gls{la} must be made clear. Afterwards, a possible measure to reduce issues related to data confidentiality is presented.

\section{Goals}
The aspiration of \gls{la} is the exploration of the learning process of students. In the scope of this project, the desired outputs of the \gls{la} may be resumed with:
\begin{itemize}
	\item Visualization of the learning path of a student.
	\item Identification of unused resources.
	\item Correlation between resource usage and final grade.
\end{itemize}

The enumerated outputs are not provided as a result of this project, but the communication layer to aggregate data. On top of this layer the different outputs may be offered at a later stage.


\section{Collected Data}
What kind of data is collected depends on the insights expected from the analysis. Until the end of this project, no concrete demand for specific learning data has been placed. As a temporary solution, data that is by default collected by the system is used. This includes events such as:

\begin{multicols}{3}
\begin{itemize}
	\item course views
	\item file uploads
	\item resource updates
	\item course completions
	\item assignment submissions
	\item grade assignments
\end{itemize}
\end{multicols}

A more detailed description of the different events is given in Section \ref{sec:moodle_plugin_events}.


\section{Ethical Concerns}
\gls{la} in the scope of this project does not collide with any ethical principles. It is used with the objective to understand the learning process of students. But \gls{la} could be used as well in other manners like estimating the chance of success or failure of a student. It is an approach that the University of Wisconsin (US) has chosen \cite{parry_2012}. One of their use cases is the suggestion of courses to enroll into based on success estimations. They even go further and propose a student that might fail his major to change it based on his performance so far.

In regard to data inaccuracy and in order to respect the autonomy of every student, researchers in the field of \gls{la} should keep in mind the consequences of their results. As the data is created and collected in the \glspl{lms} of schools and universities, the students have only low impact on the kind of data collected about them and their future usage. Hence, it is in the responsibility of researchers to assure that the results of \gls{la} do not decide over the academic career of a student.


\subsection{Confidentiality Aspects}
In the frame of this project, confidentiality may be seen as guaranteed as long as access to student data is only granted to person with the corresponding privileges and used for the initial purpose. To fulfill these aspects, the basic guidelines below are respected:

\begin{enumerate}
	\item Data from a \gls{lms} can only be retrieved by providing user credentials with the required privileges or a dedicated access token.
	\item No sensitive data about the students is exported.
	\item Before data is imported, the user should be mandated to verify that the export does not violate data privacy police of his institution\footnote{This function is not implemented but should be seen as suggestion for further improvement.}.
\end{enumerate}


\subsection{Privacy Issues}
In this context, privacy is referencing \say{to the freedom from intrusion into one's personal matters, and personal information}, according to team of FindLaw.com \cite{findlaw_2020}. Whereby "personal matters" may be replaced with the academic career of a student. 

So far, this project does not create any privacy issues. To keep it that way, the products created throughout the project could use a more restrict license than MIT, where for example the usage is limited to research purposes. Another possibility would be the enforcement of stricter anonymization which prevents de-identification of the imported data.


\section{Data Anonymization}
The general cure to privacy issues is reliable data anonymization. As stated by the General Data Protection Regulation (GDPR) of the European Union, \say{Personal data shall be kept in a form which permits identification of data subjects for no longer than is necessary for the purposes for which the personal data are processed} \cite{gdpr_2019}. 
As this project focuses on the understanding of the learning process, it is never required to being able to identify the student associated to certain interactions.
Thus, this is easily carried out by omitting unnecessary data\footnote{In the sense of not necessary data to perform the \gls{la} like names, e-mail addresses etc.} which could identify a student. 

To go a step further, the user identifier could be hashed using a random salt either before sending the data from the \gls{lms} or upon reception in the \gls{la} app to complicate the \gls{de-identification} as proposed by Spanish Agencia Española de Protección de Datos (AEPD). An alternative to the salted hash is the use of encryption with a public/private key pair.


\section{Conclusion}
The analysis uncovers not just various data confidentiality aspects, but general ethical concerns about the usage of \gls{la}. The suggested counter measures are a first step into the direction of a complete concept to reduce the risk of abuse.

Finally, data analytics and their consequences are always subject to the people performing the analysis. Hence, only they can assure a respectful handling of the data.
