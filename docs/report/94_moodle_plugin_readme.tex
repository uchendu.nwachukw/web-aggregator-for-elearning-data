\chapter{Readme: WAFED Moodle Webs Service plugin for Moodle
3.X}\label{appendix:moodle_plugin_readme}

This plugin is necessary to allow the export of course specific activity course data to the Graasp Learning Analytics App. The plugin essentially exposes 2 web services:

\begin{itemize}
\item  get\_available\_courses: returns a list of courses where the user is
  enrolled as Teacher (editingteacher)
\item
  get\_course\_data: returns the log for specified courses
\end{itemize}

In order to function properly, follow carefully the instructions below,
which will assure that:

\begin{itemize}
\item
  web services and required protocols are enabled
\item
  users can create a web token
\item
  the plugin is properly installed
\end{itemize}

\section{Configure Your Moodle
Instance}\label{configure-your-moodle-instance}

As the plugin works with web services, they have to be enabled in your
Moodle instance:

\begin{enumerate}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Access Administration \textgreater{} Site administration
  \textgreater{} Advanced features
\item
  Check `Enable web services' then click `Save Changes'
\item
  Access Administration \textgreater{} Site administration
  \textgreater{} Plugins \textgreater{} Web services \textgreater{}
  Manage protocols
\item
  Enable the REST protocol
\end{enumerate}

Additionally, you must equip the users with some extended capabilities:

\begin{enumerate}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Access Administration \textgreater{} Site administration
  \textgreater{} Users \textgreater{} Permissions \textgreater{} Define
  roles
\item
  Assign (to Authenticated Users) the capability
  \emph{moodle/webservice:createtoken} to allow the generation of a
  security key
\item
  Assign (to Authenticated Users) the capability
  \emph{webservice/rest:use} to allow the use of the communication
  protocol
\end{enumerate}

Note that assignment to Authenticated Users is only a suggestion. You
could create as well a new system role called ``Web Service User'' or
something alike.

Further information concerning the configuration of Moodle to enable web
services can be found here:
\url{https://docs.moodle.org/38/en/Using_web_services}

\section{Install Plugin}\label{install-plugin}

Now, you're ready to install the plugin. Therefore, choose one of the
below approaches. More information concerning the installation of Moodle
plugins is available on
\url{https://docs.moodle.org/39/en/Installing_plugins\#Installing_a_plugin}

\subsection{Installing Manually at the Server
(recommended)}\label{installing-manually-at-the-server-recommended}

\begin{enumerate}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Login to your webserver
\item
  Navigate to /path/to/moodle/local/
\item
  Clone this github project into the folder by executing:\\
  \texttt{git clone https://gitlab.forge.hefr.ch/uchendu.nwachukw/ 
  \\ wafed\_moodle\_webservice\_plugin.git}
\item
  In your browser, login to your Moodle as admin.
\item
  You should be notified, that additional plugins are ready to be
  installed. Confirm the database upgrade.
\end{enumerate}

Alternatively to clone the github project you may also copy the plugin
using a different approach. Just make sure that the location and folder
name are important.

\subsection{Installing via Uploaded ZIP
File}\label{installing-via-uploaded-zip-file}

\begin{enumerate}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Download the zip from this github project.
\item
  Login to your Moodle site as an admin and go to Administration
  \textgreater{} Site administration \textgreater{} Plugins
  \textgreater{} Install plugins.
\item
  Upload the ZIP file. You should only be prompted to add extra details
  (in the Show more section) if your plugin is not automatically
  detected.
\item
  If your target directory is not writable, you will see a warning
  message.
\item
  Check the plugin validation report
\end{enumerate}

\section{Test the Plugin with Postman
(optional)}\label{test-the-plugin-with-postman-optional}

You can test that all web service calls work using Postman by sending
the following specified requests to your Moodle instance.

\subsection{Request a Token}\label{request-a-token}

It is important to note that the authenticated user should be
\textbf{enrolled as Teacher (editingteacher) in at least one course}.
Furthermore, it is not possible to request a token for an admin user, so
make sure you use a regular user.

\texttt{{[}POST{]} \{\{yourMoodleUrl\}\}/login/token.php?username=\{\{yourUsername\}\}\&\\
password=\{\{yourPassword\}\}\&service=wafed\_webservices}

You should receive a response containing a \texttt{token}, which will be
used in the next requests identify the user securely. E.g.:

\begin{verbatim}
{
    "token": "a7eb3737b6b61e33991217305e8c5e59",
    "privatetoken": null
}
\end{verbatim}

\subsection{Get a List of Available
Courses}\label{get-a-list-of-available-courses}

\texttt{{[}POST{]} \{\{yourMoodleUrl\}/webservice/rest/server.php?wstoken=\{\{token\}\}\&\\
wsfunction=local\_wafed\_moodle\_webservice\_plugin\_get\_available\_courses\&\\
moodlewsrestformat=json}

You should receive a response which lists all courses where the user is
enrolled as Teacher. E.g.:

\begin{verbatim}
[
    {
        "courseid": 2,
        "shortname": "ttc",
        "userid": 4,
        "username": "john.doe",
        "roleid": 3
    },
    {
        "courseid": 4,
        "shortname": "ac",
        "userid": 4,
        "username": "john.doe",
        "roleid": 3
    }
}
\end{verbatim}

\subsection{Get the Activity Log for a specific
Course}\label{get-the-activity-log-for-a-specific-course}

\texttt{{[}POST{]} \{\{yourMoodleUrl\}/webservice/rest/server.php?wstoken=\{\{token\}\}\&\\
wsfunction=local\_wafed\_moodle\_webservice\_plugin\_get\_course\_data\&\\
moodlewsrestformat=json\&courseids{[}0{]}=\{\{courseId\}\}}

You should receive a response which contains details over actions
related to that course. E.g.:

\begin{verbatim}
{
    {
        "action": "viewed",
        "target": "course_module",
        "crud": "r",
        "contextlevel": "70",
        "edulevel": "2",
        "eventname": "\\mod_assign\\event\\course_module_viewed",
        "userid": 3,
        "role": "teacher",
        "relateduserid": null,
        "courseid": "2",
        "timecreated": 1592560102
    },
    {
        "action": "viewed",
        "target": "submission_status",
        "crud": "r",
        "contextlevel": "70",
        "edulevel": "0",
        "eventname": "\\mod_assign\\event\\submission_status_viewed",
        "userid": 3,
        "role": "teacher",
        "relateduserid": null,
        "courseid": "2",
        "timecreated": 1592560103
    }
}
\end{verbatim}

\section{You're Done! Use it with the Graasp Learning Analytics
App}\label{youre-done-use-it-with-the-graasp-learning-analytics-app}

Just by exposing these web services you will not get any wiser. Follow the
instructions on \url{https://github.com/graasp/graasp-app-moodle} to
import the data into an external application. Later this application
will allow you to perform learning analytics on your Moodle data. This
will make you wiser!
