Web Aggregator for eLearning Data
=============

General information
---------------

- **Student** : Uchendu Nwachukwu
- **Supervisor** : Sandy Ingram
- **Expert** : Juan-Carlos Farah
- **Dates** : du 18.05.2020 au 17.07.2020


Context
--------

Graasp is a platform maintained by EPFL to create eLearning courses which aim on high user interactivity. Therefore, various apps and labs can be selected from a ressource pool or created from scratch. Graasp is part of the Go-Lab Initiative which is supported by the European Union Horizon 2020 program. 


Description
-----------

One of the goals of the platform is to offer learning analytics to teachers. Therefore, low level learning data of other LMS shall be collected and displayed within a Graasp app. 
This may include data such as course/resource access frequency, test results or the navigation flow of the users within a course. Once a mechanism is put in place with another LMS, the exact scope of data to be imported into Graasp shall be defined with the corresponding person in charge.

Furthermore, the integration of Graasp courses into other LMS should be made possible. Therefore, a module should be created that handles the data exchange using the LTI standard. 
A module implementing the LTI standards may be used by all LMS that are LTI compatible. This enlarges massively the public that could profit from the courses and apps of Graasp.

Content
-------

This repo contains the whole documentation relative to the project in the folder `docs/`.

The products created in the scope of this project can be found in their corresponding repositories:

- **Moodle Plugin**: https://gitlab.forge.hefr.ch/uchendu.nwachukw/wafed_moodle_webservice_plugin.
- **Graasp App**: https://github.com/graasp/graasp-app-moodle
